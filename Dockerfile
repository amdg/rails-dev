FROM ruby:2.4

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /var/app
WORKDIR /var/app
ADD Gemfile /var/app/Gemfile
ADD Gemfile.lock /var/app/Gemfile.lock
RUN bundle install
ADD . /var/app
