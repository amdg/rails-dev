@ECHO OFF

docker-compose run web rails new . --force --database=postgresql --skip-bundle
docker-compose build
copy database.yml ..\config\database.yml
docker-compose run web rake db:create
