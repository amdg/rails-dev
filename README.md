This repository contains the files required to set up a Rails/PostgreSQL app using Docker for Windows. It is based on the official [Quickstart: Compose and Rails](https://docs.docker.com/compose/rails/) Docker documentation.

Setup
=====

  1. Download this repository from the [Downloads](https://bitbucket.org/amdg/rails-dev/downloads) section to the left. Extract the contents of the .zip file into a directory of your choice.
  2. Download and install [Docker for Windows](https://docs.docker.com/docker-for-windows/install/). If you are asked to enable Hyper-V, do so and restart your computer.
  3. Once Docker for Windows is running, right-click its whale icon ![Docker whale icon](https://amdg.bitbucket.io/images/docker.png) in your taskbar notification area. Go to Settings > Shared Drives and check the drive where you extracted the .zip file in Step 1 (usually Drive C). Click **Apply** to save your settings.
  4. Using Windows Explorer (Win + E), open the directory where you extracted the .zip file in Step 1. Enter the `_docker` subdirectory and run `setup.cmd`.
  5. Once the setup is done, you should find a basic rails app installed in your project directory. To run this on your computer, enter the `_docker` subdirectory again and run `run.cmd`. This will start the rails server in your local machine.
  6. You can now view your basic rails app at [http://localhost:3000/]. Happy coding!

This entire process should take about 30 minutes (depending on the speed of your Internet connection).

Notes
=====

  * Most of the files in the basic rails app can be edited live. However, if you edit `Gemfile`, you must run `_docker\rebuild.cmd` and restart the server.
  * If you need to run a `rails` or `rake` command, enter the `_docker` subdirectory and prefix the command with `docker-compose run web`. Example: `docker-compose run web rails generate`
